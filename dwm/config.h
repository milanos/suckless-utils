/* See LICENSE file for copyright and license details. */

/* appearance */
static const int horizpadbar        = 0;        /* horizontal padding for statusbar */
static const int vertpadbar         = 5;        /* vertical padding for statusbar */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const unsigned int borderpx  = 8;        /* border pixel of windows */
static const unsigned int gappx     = 10;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "hack:size=11", "Symbola:size=11", "EmojiOneColor-SVGinOT:size=11" };
/* static const char col_gray1[]       = "#19171c"; */
/* static const char col_gray2[]       = "#576ddb"; */
/* static const char col_gray3[]       = "#8b8792"; */
/* static const char col_gray4[]       = "#19171c"; */
/* static const char col_cyan[]        = "#576ddb"; */
/* static const char *colors[][3]      = { */
/* 	/1*               fg         bg         border   *1/ */
/* 	[SchemeNorm] = { col_gray3, col_gray1, col_gray4 }, */
/* 	[SchemeSel]  = { col_gray4, col_cyan,  col_gray2  }, */
/* }; */

static const char color00[]="#1C1E26"; // Base 00 - Black
static const char color01[]="#E93C58"; // Base 08 - Red
static const char color02[]="#EFAF8E"; // Base 0B - Green
static const char color03[]="#EFB993"; // Base 0A - Yellow
static const char color04[]="#DF5273"; // Base 0D - Blue
static const char color05[]="#B072D1"; // Base 0E - Magenta
static const char color06[]="#24A8B4"; // Base 0C - Cyan
static const char color07[]="#CBCED0"; // Base 05 - White
static const char color08[]="#676A8D"; // Base 03 - Bright Black
/* static const char color09[]=color01; // Base 08 - Bright Red */
/* static const char color10[]=color02; // Base 0B - Bright Green */
/* static const char color11[]=color03; // Base 0A - Bright Yellow */
/* static const char color12[]=color04; // Base 0D - Bright Blue */
/* static const char color13[]=color05; // Base 0E - Bright Magenta */
/* static const char color14[]=color06; // Base 0C - Bright Cyan */
static const char color15[]="#E3E6EE"; // Base 07 - Bright White
static const char color16[]="#E58D7D"; // Base 09
static const char color17[]="#E4A382"; // Base 0F
static const char color18[]="#232530"; // Base 01
static const char color19[]="#2E303E"; // Base 02
static const char color20[]="#CED1D0"; // Base 04
static const char color21[]="#DCDFE4"; // Base 06
static const char color_foreground[]="#CBCED0"; // Base 05
static const char color_background[]="#1C1E26"; // Base 00

/* static const char norm_fg[] = base0F; */
/* static const char norm_bg[] = base00; */
/* static const char norm_border[] = base03; */

/* static const char sel_fg[] = base0F; */
/* static const char sel_bg[] = base03; */
/* static const char sel_border[] = base01; */

static const char *colors[][4]      = {
    /*               fg           bg         border          floating                    */
    [SchemeNorm] = { color15,     color18,   color19,	 color03}, // unfocused wins
    [SchemeSel]  = { color15,     color19,    color01,   color04},  // the focused win
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "Gimp",    NULL,     NULL,           0,         1,          0,           0,        -1 },
	{ "Firefox", NULL,     NULL,           1 << 2,    0,          0,          -1,        -1 },
	{ "st",      NULL,     NULL,           0,         0,          1,          -1,        -1 },
	{ NULL,      NULL,     "Event Tester", 0,         1,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

//#include "fibonacci.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
	{ MOD, XK_j,     ACTION##stack, {.i = INC(+1) } }, \
	{ MOD, XK_k,     ACTION##stack, {.i = INC(-1) } }, \
	{ MOD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \
	{ MOD, XK_BackSpace,     ACTION##stack, {.i = 0 } }, \
	{ MOD, XK_a,     ACTION##stack, {.i = 1 } }, \
	{ MOD, XK_z,     ACTION##stack, {.i = 2 } }, \
	{ MOD, XK_x,     ACTION##stack, {.i = -1 } },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, NULL };

static const char *browsercmd[] = { "firefox", NULL };
static const char *termcmd[]  = { "st", NULL };

static const char *brightup[]   = { "brightup",   NULL };
static const char *brightdown[] = { "brightdown", NULL };

static const char *upvol[]   = { "volup",    NULL };
static const char *downvol[] = { "voldown",  NULL };
static const char *mutevol[] = { "volmute",  NULL };

static const char *slock[] = { "slock", NULL };

static const char *refreshstatus[] = { "dwmrefresh", NULL };

static const char *mpdnext[] = { "dwm_mpd_cmd", "next", NULL };
static const char *mpdprev[] = { "dwm_mpd_cmd", "prev", NULL };
static const char *mpdtoggle[] = { "dwm_mpd_cmd", "toggle", NULL };
static const char *mpdstop[] = { "dwm_mpd_cmd", "stop", NULL };

static const char *musiccmd[] = { "st", "-e", "ncmpcpp", NULL };
static const char *mailcmd[] = { "st", "-e", "neomutt", NULL };
static const char *passcmd[] = { "passmenu", NULL };
static const char *mpdmenucmd[] = { "mpdmenu", NULL };


static Key keys[] = {

	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,			            XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,			    XK_p,      spawn,          {.v = passcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	STACKKEYS(MODKEY,                          focus)
	STACKKEYS(MODKEY|ShiftMask,                push)
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,			            XK_g,	   zoom,           {0} },
	{ MODKEY|ShiftMask,             XK_w,      spawn,          {.v = browsercmd } },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,						XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,				XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,			            XK_f,      fullscreen,     {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ MODKEY,                       XK_s,      togglesticky,   {0} },

	{ MODKEY|ShiftMask,             XK_comma,      spawn,          {.v = mpdprev } },
	{ MODKEY|ShiftMask,             XK_period,     spawn,          {.v = mpdnext } },
	{ MODKEY|ShiftMask,             XK_m,          spawn,          {.v = mpdmenucmd } },

	{ 0,          XF86XK_AudioStop,            spawn,          {.v = mpdstop   } },
	{ 0,          XF86XK_AudioPlay,			   spawn,          {.v = mpdtoggle } },

	{ 0,          XF86XK_MonBrightnessUp,	   spawn,          {.v = brightup} },
	{ 0,          XF86XK_MonBrightnessDown,	   spawn,          {.v = brightdown} },

	{ 0,          XF86XK_AudioLowerVolume,	   spawn,          {.v = downvol } },
	{ 0,          XF86XK_AudioMute,            spawn,          {.v = mutevol } },
	{ 0,          XF86XK_AudioRaiseVolume,     spawn,          {.v = upvol   } },

	{ 0,          XF86XK_HomePage,             spawn,          {.v = refreshstatus } },


};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

