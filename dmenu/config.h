/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"monospace:size=10"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
char base00[] = "#1C1E26";
char base01[] = "#232530";
char base02[] = "#2E303E";
char base03[] = "#6F6F70";
char base04[] = "#9DA0A2";
char base05[] = "#CBCED0";
char base06[] = "#DCDFE4";
char base07[] = "#E3E6EE";
char base08[] = "#E95678";
char base09[] = "#FAB795";
char base0A[] = "#FAC29A";
char base0B[] = "#29D398";
char base0C[] = "#59E1E3";
char base0D[] = "#26BBD9";
char base0E[] = "#EE64AC";
char base0F[] = "#F09383";

static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = {base07, base00},
	[SchemeSel] = {base0F, base01},
	[SchemeOut] = {base0F, base03},
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;
static unsigned int lineheight = 28;         /* -h option; minimum height of a menu line     */

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
