#!/bin/sh
FAILED=""
for d in $@; do
	make -C $d install clean || FAILED="$FAILED $d"
done
echo "FAILED: $FAILED"
